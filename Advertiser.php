<?php
/**
 * Created by PhpStorm.
 * User: cosmi
 * Date: 13-Oct-18
 * Time: 9:25 AM
 */

namespace App\Models;

/**
 * Advertiser entity
 * Class Advertiser
 * @package App\Models
 */
class Advertiser
{
    private $id;
    private $advertiserName;
    private $storeName;
    private $defaultCommission;
    private $url;
    private $description;
    private $status;
    private $mainUrl;
    private $programId;
    private $imageUrl;
    private $conditions;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getAdvertiserName()
    {
        return $this->advertiserName;
    }

    /**
     * @param mixed $advertiserName
     */
    public function setAdvertiserName($advertiserName)
    {
        $this->advertiserName = $advertiserName;
    }

    /**
     * @return mixed
     */
    public function getStoreName()
    {
        return $this->storeName;
    }

    /**
     * @param mixed $storeName
     */
    public function setStoreName($storeName)
    {
        $this->storeName = $storeName;
    }

    /**
     * @return mixed
     */
    public function getDefaultCommission()
    {
        return $this->defaultCommission;
    }

    /**
     * @param mixed $defaultCommission
     */
    public function setDefaultCommission($defaultCommission)
    {
        $this->defaultCommission = $defaultCommission;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getMainUrl()
    {
        return $this->mainUrl;
    }

    /**
     * @param mixed $mainUrl
     */
    public function setMainUrl($mainUrl)
    {
        $this->mainUrl = $mainUrl;
    }

    /**
     * @return mixed
     */
    public function getProgramId()
    {
        return $this->programId;
    }

    /**
     * @param mixed $programId
     */
    public function setProgramId($programId)
    {
        $this->programId = $programId;
    }

    /**
     * @param mixed $imageUrl
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @param mixed $conditions aka tos
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }
    
}
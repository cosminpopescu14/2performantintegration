<?php
/**
 * Created by PhpStorm.
 * User: cosmi
 * Date: 08-Oct-18
 * Time: 10:13 PM
 */

namespace App\Integration;
require 'vendor/autoload.php';
require 'Config.php';
require 'Utils.php';
require 'Advertiser.php';
require 'ConfigurationException.php';

use App\Utils\Utils;
use Monolog\Handler\StreamHandler;
use TPerformant\API\Filter\AffiliateProgramFilter;
use TPerformant\API\HTTP\Affiliate;
use TPerformant\API\Exception;
use App\Config;
use App\Models;
use Monolog\Logger;


class TwoPerformantOperations
{

    private $config;
    private $log;

    public function __construct(Config\Config $_config, Logger $logger)
    {
        $this->config = $_config;
        $this->log = $logger;
    }



    private function initAffiliate()
    {
        $this->log = new Logger(TwoPerformantOperations::class);
        $this->log->pushHandler(new StreamHandler("app.log", Logger::DEBUG));
        $affiliate = null;

        try
        {
            $affiliate = new Affiliate($this->config->getEmail(), $this->config->getPassword());
        }
        catch (Exception\APIException $ex)
        {
            $this->log->err($ex->getMessage());
        }
        return $affiliate;
    }

    /**
     * @author Popescu Cosmin Ionut
     * @email cosmin.popescu93@gmail.com
     */
    public function getAdvertisers($page)
    {
        $advertisers = [];
        try
        {
            $apiResponse = $this->initAffiliate()->getPrograms((new AffiliateProgramFilter)->page($page));

            foreach ($apiResponse as $response)
            {
                $advertiser = new Models\Advertiser();

                $advertiser->setStatus($response->getStatus());
                $advertiser->setId($response->getId());
                $advertiser->setAdvertiserName($response->getName());
                $advertiser->setDefaultCommission($response->getDefaultSaleCommissionRate());
                $advertiser->setUrl($this->convertToAffiliateLink($response->getMainUrl(), $response->getUniqueCode()));
                $advertiser->setDescription($response->getDescription());
                $advertiser->setStatus($response->getStatus());
                $advertiser->setMainUrl($response->getmainUrl());
                $advertiser->setImageUrl($response->getlogoPath());
                $advertiser->setConditions($response->getTos());
                $advertiser->setProgramId($response->getAffRequest()->id);

                $advertisers[] = [
                                    "id" => $advertiser->getId(),
                                    "advertiseName" => $advertiser->getAdvertiserName(),
                                    "defaultComision" => $advertiser->getDefaultCommission(),
                                    "status" => $advertiser->getStatus(),
                                    "affiliateLink" => $advertiser->getUrl(),
                                    "description" => Utils::clean($advertiser->getDescription()),
                                    "mainUrl" => $advertiser->getMainUrl(),
                                    "image" => $advertiser->getImageUrl(),
                                    "conditions" => Utils::clean($advertiser->getConditions()),
                                    "programId" => $advertiser->getProgramId()
                                 ];
            }

            $this->log->info("", ["got" => count($advertisers)]);
            return $advertisers;
        }
        catch (\App\Exception\ConfigurationException $configurationException)
        {
            $this->log->err($configurationException->getMessage());
        }
    }

    private function convertToAffiliateLink($link, $program)
    {
        return $this->initAffiliate()->getQuicklink($link, $program);
    }
}